##Automating red team attacks in cyber ranges

Information systems security is becoming more and more important year
by year. The use of computer networks has increased in all sectors and
due to the coronavirus pandemic, the transition of many activities from
the physical to the digital world has been accelerated. However, along
with the increase in the use of networks, there is also an increase in
cybercrime.

The purpose of this dissertation is to study the ways in which a red team
examines a system in order to identify vulnerable and weak points. The
literature review revealed that the existing tools lag behind in usability,
simplicity and automation.

For this reason, the implementation of the automated tool "ARTAS" was
deemed necessary, in order to provide an additional tool to the
information systems security professionals. The "ARTAS" tool has been
successfully tested in a cyber-range environment and significantly
improves the usability and simplicity of automated procedures.