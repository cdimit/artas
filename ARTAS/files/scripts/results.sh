#!/bin/bash

out=`ls ./files/output/ | tr -s ' ' '\n'`
n=1
while read -r line; 
do
echo "$n) $line"
n=$((n+1))
done < <(printf '%s\n' "$out")

echo "Choose number to show the result:"
read res
n=1
while read -r line; 
do
if [[ "$res" == "$n" ]]
then
	cat ./files/output/$line
fi
n=$((n+1))
done < <(printf '%s\n' "$out")


