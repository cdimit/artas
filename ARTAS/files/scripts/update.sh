#!/bin/bash

echo "Tools:"
echo "nmap - Network exploration tool and security / port scanner"
echo "wpscan - WordPress Security Scanner"
echo "nikto - Scan web server for known vulnerabilities"
echo "gobuster -Tool used to brute-force URIs including directories and files as well as DNS subdomains"
echo "goldeneye - HTTP DoS test tool"
echo "hashcat - Advanced CPU-based password recovery utility"
echo ""

echo -n "Do you want to update the tools? [Y]es, [N]o:"
read answer
if [[ $answer = "Y" ]] || [[ $answer = "Yes" ]] || [[ $answer = "y" ]]
then
  sudo apt update
  sudo apt upgrade nmap wpscan nikto gobuster goldeneye hashcat -y
fi