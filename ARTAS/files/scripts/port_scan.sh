#!/bin/bash

ip=localhost

echo -n "Target URL:"
read ip
echo "Scan type:"
echo "1) TCP [Default]"
echo "2) SYN"
echo "3) UDP"
echo "4) NULL"
echo "5) FIN"
echo "6) Xmas"
echo 
echo -n "Enter choice: " 
read scan_type
case $scan_type in
	2) scan_type="-sS" ;;
	3) scan_type="-sU" ;;
	4) scan_type="-sN" ;;
	5) scan_type="-sF" ;;
	6) scan_type="-sX" ;;
	*) scan_type="-sT" ;;
esac

echo ""
echo "Flags:"
echo "0) None [Default]"
echo "1) Treat all hosts as online"
echo "2) Aggresive Scan"
echo "3) OS detection"
echo "4) Version detetion"
echo "5) Scan ports from 1 through 65535"
echo "6) Firewall Evasion: Generate invalid checksum for packets"
echo "7) Firewall Evasion: Fragment the packets into smaller pieces"
echo 
echo -n "Enter choice: " 
read flag
case $flag in
	0) flag="" ;;
	1) flag="-Pn" ;;
	2) flag="-A" ;;
	3) flag="-O" ;;
	4) flag="-sV" ;;
	5) flag="-p-" ;;
	6) flag="--badsum" ;;
	7) flag="-f" ;;
esac

echo ""
echo "Time (from slower to faster):"
echo "1) Paranoid" 
echo "2) Sneaky"
echo "3) Polite"
echo "4) Normal [Default]"
echo "5) Aggressive"
echo "6) Insane"
echo 
echo -n "Enter choice: " 
read time
case $time in
	1) time="-T0" ;;
	2) time="-T1" ;;
	3) time="-T2" ;;
	5) time="-T4" ;;
	6) time="-T5" ;;
	*) time="-T3" ;;
esac

nmap $scan_type $flag $time $ip -oN ./files/output/$(date "+%Y-%m-%d_%H:%M:%S")_nmap


