#!/bin/bash

ip=localhost
mask=16

echo -n "Target IP [Default=localhost]:"
read ip
if [[  -z "$ip" ]]
then
	ip="localhost"
fi

echo ""
echo "Subnet Mask:"
echo "1) 8"
echo "2) 16"
echo "3) 24 [Default]"
echo 
echo -n "Enter choice: " 
read mask
case $mask in
	1) mask="8";;
	2) mask="16" ;;
	*) mask="24" ;;
esac

nmap -sn $ip/$mask  #Ping Sweep


