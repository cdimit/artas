#!/bin/bash


echo -n "Insert Hash:"
read hash
echo $hash > /tmp/artas.hash
echo "Hash type:"
echo "1) MD5"
echo "2) SHA1"
echo "3) MD4"
echo "4) SHA-256"
echo "5) bcrypt"
echo "6) NTLM"
echo 
echo -n "Enter choice: " 
read type
case $type in
	1) type="0" ;;
	2) type="100" ;;
	3) type="900" ;;
	4) type="1400" ;;
	5) type="3200" ;;
	*) type="1000" ;;
esac

hashcat -m $type /tmp/artas.hash ./files/wordlists/rockyou.txt

hashcat -m $type /tmp/artas.hash --show > /tmp/artas.hashed

if [ -s /tmp/artas.hashed ]; then
        # The file is not-empty.
        cat /tmp/artas.hashed > ./files/output/$(date "+%Y-%m-%d_%H:%M:%S")_hashcat.txt
        cat ./files/output/$(date "+%Y-%m-%d_%H:%M:%S")_hashcat.txt
else
        echo "No Results"
fi