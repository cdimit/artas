#!/bin/bash

ip=localhost

echo -n "Target URI: http://"
read ip
echo -n "Wordlist: [C]ommon (default), [S]mall, [B]ig:"
read wordlist
case $wordlist in
	s|S) wordlist=./files/wordlists/small.txt;;
	b|B) wordlist=./files/wordlists/big.txt;;
	*) wordlist=./files/wordlists/common.txt;;
esac

echo -n "File extensions: [Y]es, [N]o (default)"
read ext
case $ext in
	y|Y) gobuster dir -x txt,php,html -u $ip -w $wordlist -o ./files/output/$(date "+%Y-%m-%d_%H:%M:%S")_gobuster.log;;
	*) gobuster dir -u $ip -w $wordlist -o ./files/output/$(date "+%Y-%m-%d_%H:%M:%S")_gobuster.log;;
esac

