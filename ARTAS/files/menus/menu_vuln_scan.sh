#!/bin/bash


quit=n 
while [  "$quit"   =   "n"  ] 
do 
#clear 
echo
echo 
echo "Vulnerability Scan"
echo "1. Brute-Force directories/files in web server"
echo "2. Web Server Vulnerability Scanner"
echo "3. WordPress Vulnerability Scanner"
echo "H. Help"
echo "Q. Back" 
echo 
echo -n "Enter choice: " 
read choice 
case $choice in 
1)./files/scripts/./gobuster.sh
  ;;
2)./files/scripts/./nikto.sh
  ;;
3)./files/scripts/./wpscan.sh
  ;;
H|h)cat ./files/help/vulnerability_scan.txt
  ;;
Q|q) quit=y;; 
*) echo "Try Again" 
sleep 1 
esac 
done 

