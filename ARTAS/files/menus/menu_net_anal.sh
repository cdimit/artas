#!/bin/bash


quit=n 
while [  "$quit"   =   "n"  ] 
do 
#clear 
echo
echo 
echo "Network Analysis"
echo "1. Ping Sweep" 
echo "2. Port Scan" 
echo "H. Help"
echo "Q. Back" 
echo 
echo -n "Enter choice: " 
read choice 
case $choice in 
1)./files/scripts/./ping_sweep.sh
  ;;
2)./files/scripts/./port_scan.sh
  ;;
H|h)cat ./files/help/network_analysis.txt
  ;;
Q|q) quit=y;; 
*) echo "Try Again" 
sleep 1 
esac 
done 

