#!/bin/bash


quit=n 
while [  "$quit"   =   "n"  ] 
do 
#clear 
echo
echo 
echo "Main Menu:"
echo "1. Network Analysis" 
echo "2. Vulnerability Scan" 
echo "3. Attacks"
echo "4. Results"
echo "5. Tools/Update"
echo "Q. Quit" 
echo 
echo -n "Enter choice: " 
read choice 
case $choice in 
1)./files/menus/./menu_net_anal.sh
  ;;
2)./files/menus/./menu_vuln_scan.sh
  ;;
3)./files/menus/./menu_attacks.sh
  ;;
4)out=`ls ./files/output/`
  if [ -z "$out" ]
    then
        echo "No Results"
    else
        ./files/scripts/results.sh
    fi
  ;;
5)./files/scripts/update.sh
  ;;
Q|q) quit=y;; 
*) echo "Try Again" 
sleep 1 
esac 
done 
echo "Thank you, Come again"

